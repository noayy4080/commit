<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//users routes
Route::resource('users', 'UsersController')->middleware('auth');
Route::get('users/{id}/edit', 'UsersController@edit')->name('users.edit')->middleware('auth');
Route::post('users/{id}/update/', 'UsersController@update')->name('users.update')->middleware('auth');
Route::get('users/changerole/{id}/{rid}', 'UsersController@changerole')->name('users.changerole')->middleware('auth');

Route::get('users/add', '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('users.add')->middleware('auth');
Route::post('users/create', '\App\Http\Controllers\Auth\RegisterController@adduser')->name('users.create')->middleware('auth');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
